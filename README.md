# jenkins ECS
ref: [https://www.slideshare.net/PhilippKoch11/never-stand-in-row-again-with-jenkins-on-aws-ecs-and-fargate](https://www.slideshare.net/PhilippKoch11/never-stand-in-row-again-with-jenkins-on-aws-ecs-and-fargate)

![](./imgs/infra-jenkins.png)
![](./imgs/aws-efs-backup-solutions.png)

## Usando terraform
- infra rede
    - SG
    - IG
    - VPC
    - Sub
    - RT

- infra cluster ECS
    - Cluster Prod
    - Cluster Staging

- infra TaskDefinition
    - ECS FARGATE CLUSTER
    - EC2 FARGATE CLUSTER ( com AutoScaling Group)
    - **(verificar como colocar os logs)**

- infra up Jenkins master
    - Certificate for Jenkins DNS
    - Applications Load Balancer (Jenkins master)
    - Network Load Balancer (Jenkins Slave)
    - ecr
    - efs volume
    - efs backup
        - cloudwatch
        - lambda
        - sns
        - dynamoDb
        - S3
        - https://aws.amazon.com/pt/solutions/implementations/efs-to-efs-backup-solution/
        - https://github.com/aws-solutions/efs-backup

